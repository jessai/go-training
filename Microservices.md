# Go - Micro-services

- Independent and easily deployable service
- Can easily communicate an collaborate with other services
- Responsible for one specific task
- Micro-services help build complex scalable applications

## Architecture

|   Persistence Layer _(db connection)_    |
| :--------------------------------------: |
|            Service main code             |
| Configuration \| Communication _(http, tcp, etc)_ |

## File Structure

```text
.
+--Proyect
|	+--databaselayer
| 		+-- db.go
|	+--proyectcode
|		+--proyectcode.go
|	+--extralayer
|	main.go
```





