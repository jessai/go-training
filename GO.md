#GO

## Encoding

%d - decimal

%b - binary

%x - hexadecimal

%q - string utf-8 ascii

%p - base 16 notation

%v - print value of variable

* Formatting verbs
  * %#x - add 0xLowercase
  * %#X - add 0xUppercase
* Scape characters
  * \t - tab
  * \n - new line
* No pollution

## Packages

* One folder - many files
* package declaration in every files
* package scope
  * something in one file is accessible to another file 

  * Lowercase is not exported (unexported - no visible)

  * Uppercase is exported outside the package

  * An experted function can use a lowercase function

  *  import ("myFolder") -> myFolder.ExportedFunction

     ```go
     //name.go
     package vis

     var MyName = "Jess"		//variable exported (uppercase)
     var yourName = "xxx"	//variable not exported (lowercase)
     ```

     ​

     ```go
     // main.go
     package main 
     import(
     	"fmt"
       	"mypackage/vis"
     )

     func main() {
       fmt.Println(vis.MyName)	// variable MyName accessable
       vis.Println(vis.yourName) // error: var not accessable
     }
     ```

     ​

## Variables

* shorthand

  * can only be used inside func

    ```go
    a := 10
    ```

    ​

* var

  * var b string = "my string"

  * zero value

    ``` go
    var a int		// 0
    var b string	// ""
    var c float64	// 0
    var d bool		// false
    ```

* type format verb: %T

  * -> return the type of variable

* declare -> assign -> initialize

* multiple declaration & assign

  ``` go
  var b, c string = "string for b", "string for c"

  j, k, l, m := 44.7, true, false, "m"
  ```

## Scope 

keep your scope tight

### Levels

* universe
* package
  * for variables
  * not for imports
* file
  * imports
* block (curly braces)

### Package scope

``` go
package main
import "fmt"

// x variable can be used in any file on the package
var x int = 42 

func main() {
  fmt.Println(x)
  foo()
}

func foo() {
  fmt.Println(x)
}
```

### Block scope

```go
package main
import "fmt"

func wrapper() func() int {	// a function that returns a function that return an int
  x := 0
  return func() int {
      x++
      return x
  }
}

func main() {
  increment := wrapper()	// assign a function to variable
  fmt.Println(increment())	// 1
  fmt.Println(increment())	// 2
}
```

## Blank identifier

* You must use everything you put in your code (no pollution)
  * if you declare a variable, you must use it
* The blank identifier
  * _
  * allows us to tell the compiler we aren't using something

``` go
package main

import (
  "net/http"
  "io/ioutil"
  "fmt"
)

func main() {
  res, _ := http.Get("http://www.mcleods.com")	// assign an error callback to _ 
  page, _ := ioutil.ReadAll(res.Body)
  res.Body.Close()
  fmt.Printf("%s", page)
}
```

## Constants

Single declaration

```go
const p string = "my string"	// Uppercased name is not a convention (STRING)
```

Multiple declaration

```go
const (
	Pi = 3.1416	//Capitalization is a convention
  	Lan = "GO"
)
```

iota

```go
const (
	A = iota //0
  	B = iota //1
  	C = iota //2
)
```

iota use

```go
const (
	_ = iota 	//0
  Kb = 1 << (iota * 10) // 1 << (1*10)
  Mb = 1 << (iota * 10) // 1 << (2*10)
  // bit shifting
)
```

A constant is a parallel type system

* C / C++ has problems with a lack of strict typing

* in Go, there is no mixing of numeric types

  ```go
  const hello = "Hello World"		//untyped variable
  const typedHello string = "Hello World" //typed variable 
  ```

An **untyped** constant value does not yet  have a  fixed type, it's a *"kind"* , not yet forced to obey the strict rules that prevent combining differently typed values. Example:

What is the type of 42? -> int? -> uint? -> float64?

If didn't have **untyped constants**, then we would have to do conversion on every literal value: int(42) 

## Memory addresses (Pointers) 

### Memory address

```go
package main
import "fmt"

func main() {
  a := 42
  
  fmt.Println("a - ", a)
  fmt.Println("a's memory address - ", &a)
}
```

---

Example:

```go
package main
import "fmt"

const metersToYards float64 = 1.09361
func main() {
  var meters float64
  
  fmt.Print("Enter meters swam: ")
  fmt.Scan(&meters)
  
  yards := meters * metersToYards
  
  fmt.Println(meters, " meter is ", yards, " yards.")
}
```

### Pointers

####Referencing

```go
package main
import ("fmt")

func main() {
  a:= 42
  fmt.Println(a)	//value
  fmt.Println(&a)	//memory address of variable a
  
  var b *int = &a	//variable type **int pointer**
  
  fmt.Println(b)
}
```

#### Dereferencing

```go
package main
import ("fmt")

func main() {
  a:= 42
  fmt.Println(a)	//42
  fmt.Println(&a)	//0x12345
  
  var b *int = &a

  fmt.Println(b)	//0x12345
  fmt.Println(*b)	//42
}
```

Passing memory addresses instead of a bunch of values makes our programs more performant. We don't have have to pass around large amounts of data, ==we only have to pass addresses==.

Everything is **passed by value** in **go**. When we pass a memory address, we are passing a value.

---

Example:

_No pointers_

```go
package main
import "fmt"

func zero(z int) { // x is passed "by copy" -> "by value"
    z = 0
}

func main() {
  x := 5
  zero(x)
  fmt.Println(x) // x is still 5
}
```

_Pass by value (duplicated values/variable)_

```go
package main
import "fmt"

func zero(z int) {
  fmt.Printf("%p\n", &x) 	// address in func zero 0x1234
  fmt.Println(&x)
  z = 0
}

func main() {
  x := 5
  fmt.Printf("%p\n", &x)	// address in func main 0x5678
  fmt.Println(&x)
  zero(x)
  fmt.Println(x)			// x is still 5
}
```

_With Pointers_

```go
package main
import "fmt"

func zero(z *int) {
  fmt.Println(z)	// the address is the same, not duplicated values
   *z = 0			
}

func main() {
  x := 5
  zero(&x)
  fmt.Println(&x)
  fmt.Println(x)	// x is 0
}
```

## Loops

### For

```go
//Like a C for
for init; condition; post {}

//Like a C while
for condition {}

// Like a C for (;;) always running
for {}
```

---

Examples:

```go
// Like for
sum := 0
for i := 0; i < 10; i++ {
    sum += 1
}
```

```go 
// Like while
i := 0
for i < 10 {
  fmt.Println(i)
  i++
}
```

```go
// no condition
i := 0
for {
  fmt.Println(i)
  i++
}
```

```go
package main
import "fmt"

func main() {
  i :=0
  for {
    i++
    if i%2 == 0 {
        continue	// stop and continue the loop
    }
    fmt.Println(i)	// 1,3,5,...,51
    if i >= 50 {
        break
    }
  }
}
```

##Rune

- Single character:  'a'
- Alias for int32
- An integer value identifying a Unicode code point

###Conversion (casting)

```go
package main
import "fmt"

func main(){
  fmt.Println([]byte("Hello"))	// [72 101 108 108 111]
}
```



## Switch

Single condition

```go
package main
import "fmt"

func main() {
  switch "Matt" {
    case "Zac":
    	fmt.Println("Hi Zac")
    case "Matt":
    	fmt.Println("Hi Matt")
  default:						// optional state
    	fmt.Println("Default")
  }
}
```

Fallthrough

```go
func main() {
  switch "Zac" {
    case "Zac":
    	fmt.Println("Hi Zac")
    	fallthrough				// pass true to the next case
    case "Matt":
    	fmt.Println("Hi Matt")
  default:						
    	fmt.Println("Default")
  }
}
```



Multiple conditions

```go
func main() {
    switch "Matt" {
    case "Zac":
    	fmt.Println("Hi Zac")
    case "Paul", "Matt":		// if Paul or Matt is true
    	fmt.Println("Hi Paul or Matt")
  default:
    	fmt.Println("Default")
  }
}
```

No expression

```go
func main() {
  myFriendsName := "Mar"
  
  switch {
    case len(myFriendsName) == 2:
    	fmt.Println("Name of lenght 2")
    case myFriendsName == "Tim":
    	fmt.Println("Hi Tim")
    case myFriendsName == "Marcus", myFriendsName == "Medhi":
    	fmt.Println("Your name is either Marcus or Medhi")
  }
  
}
```

On Type

```go
func main() {
  switch x.(type) { 		//asserting; "x is of this type"
    case int:
    	fmt.Println("int")
    case string:
    	fmt.Println("string")
    default:
    	fmt.Println("Unknown")
  }
}
```

## Conditions

Simple

```go
func main() {
  if true {
    fmt.Println("true")
  } else {
    fmt.Println("false")
  }
}
```

Initialization statement

```go
if err := file.Chmod(0664); err != nil { //init var and condition
  fmt.Println(err) // scope of err is just inside if
  return err
}
```

## Functions

Main is the entry point to the program

```go
package main
import "fmt"

func main() {
  fmt.Println("Hello world")
}
```

### Parameters

Single parameter

```go
package main
import "fmt"

func main(){
  greet("jane")
  greet("Jhon")
}

func greet(name string) {
  fmt.Prinln(name)
}
```

Multiple parameters

```go
package main
import "fmt"

func main(){
  greet("jane","june")
}

func greet(fname string, lname string) {
  fmt.Prinln(fname, lname)
}
```

Returning

```go
package main
import "fmt"

func main(){
  fmt.Prinln(greet("jane","june"))
}

func greet(fname string, lname string) string { // type of return
  return fmt.Sprint(fname, lname)
}
```

Returning name value

```go
package main
import "fmt"

func main() {
  fmt.Println(greet("jane ", "june"))
}

func greet( fname string, lname string) (s string) {
  s = fmt.Sprint(fname, lname)
  return
}
```

Returning multiple values

```go
package main
import "fmt"

func main() {
  fmt.Println(greet("jane ", "june"))
}

func greet(fname, lname string) (string, string) {
  return fmt.Sprin(fname, lname), fmt.Sprint(lname, fname)
}
```

Variadic functions

Variable prefixed with ...

```go
package main
import "fmt"

func main() {
  n := average(43, 56, 87, 12, 45, 57) 
  fmt.Println(n)
}

func average(sf ...float64) float64 { //variadic parameters
  total := 0.0
  for _, v:= range sf {
      total += v
  }
  return total / float64(len(sf))
}
```

```go
package main
import "fmt"

func main() {
  data := []float64 {43, 56, 87, 12, 45, 57}
  n := average(data...) //variadic arguments
  // passed one by one element in data
  fmt.Println(n)
}

func average(sf ...float64) float64 {
  total := 0.0
  for _, v:= range sf {
      total += v
  }
  return total / float64(len(sf))
}

```

## Functions

Basic form

```go
func receivers identifier parameters returns { ... }
```

Function expression

```go
package main
import "fmt"

func main() {
  greeting := func() {				// type func()
    fmt.Println("Hello World")
  }
  
  greeting()
}
```

```go
package main
import "fmt"

func makeGreeter() func() string { //returns a func() string
  return func() string { //anonymous function returns string
      	return "Hello world"
  }
}

func main() {
  greet := makeGreeter() // type func() string
  fmt.Println(greet())
}
```

Callbacks

```go
package main
import "fmt"

func visit(numbers []int, callback funct(int)) {
  for _, n := range numbers {
    callback(n)
  }
}

func main() {
  visit([]int {1, 2, 3, 4}, func(n int) {
    fmt.Println(n)
  })
}
```

```go
package main
import "fmt"

func filter(numbers []int, callback func(int) bool) []int {
  xs := []int{}
  for _, n := range numbers {
    if callback(n) {
      xs = append(xs, n)
    }
  }
  return xs
}

func main() {
  xs := filter([]int{1, 2, 3, 4}, func(n int) bool {
      return n > 1
  })
  fmt.Println(xs)
}
```

Recursion

```go
package main
import "fmt"

func factorial(x int) int {
  if x == 0 {
      return 1
  }
  return x* factorial(x-1)
}

func main() {
  fmt.Println(factorial(4))
}
```

Defer

```go
package main
import "fmt"

func hello() {
  fmt.Prinln("hello")
}

func world() {
  fmt.Println("wolrd")
}

func main() {
  defer world()		//this run at last. Before exit
  hello()
}					// hello world
```

Reference types (pass by value)

```go
package main
import "fmt"

func main() {
  m := make([]string, 1, 25)
  fmt.Println(m) // []
  changeMe(m)
  fmt.Println(m) // [Todd]
}

func changeMe(z []string) {
  z[0] = "Todd"
  fmt.Println(z) // [Todd]
}
```

Anonymous Self - Executing Function

```go
func main() {
  func () {
    fmt.Println("Hello world")
  }()
}
```

## Data Structures

###Array

Array is a numbered sequence of elements of a single type.

Is not dynamic: **does not change in size**

```go
var x [128]string //len 128
```

### Slice

- It is a reference type
- The init value is nil
- Is **dynamic**

Form

```go
make([]Type, len, capacity)
```

Basic

```go
package main
import "fmt"

func main() {
  mySlice := []int{1, 3, 5, 7}	//basic slice
  fmt.Printf("%T\n", mySlice)	//[]int
  fmt.Println(mySlice)			//[1 3 5 7]
}
```



Slicing

```go
package main
import "fmt"
func main() {
  mySlice := []string{}"b", "b", "c", "d", "f", "g"}
	fmt.Println(mySlice)		
	fmt.Println(mySlice[2:4])	//[c d] -> slicing from 2-4
	fmt.Println(mySlice[:4])	//[a b c d]
	fmt.Println(mySlice[2:])	//[c d f g]
	fmt.Println(mySlice[2])		//c
	fmt.Println("myString"[2])	//83
}
```

Appending

```go
package main
import "fmt"

func main(){
  mySlice := []string{"Monday", "Tuesday"}
  myOtherSlice := []string{"Wednesday", "Thursday", "Friday"}
  
  mySlice = append(mySlice, myOtherSlice...)
  fmt.Println(myslice)
}
```

Deleting from slice

```go
package main
import "fmt"

func main(){
  mySlice := []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"}
  
  mySlice = append(mySlice[:2], mySlice[3:]...)
  fmt.Println(myslice)
}
```

Multidimensional

```go
package main
import "fmt"

func main() {
  records := make([][]string, 0)
  
  // record 1
  rec1 := make([]string, 4)
  rec1[0] = "Foster"
  rec1[1] = "Nathan"
  rec1[2] = "100.00"
  rec1[3] = "74. 00"
  records = append(reccords, rec1)
  
   // record 2
  rec2 := make([]string, 4)
  rec2[0] = "Dode"
  rec2[1] = "Lisa"
  rec2[2] = "92.00"
  rec2[3] = "96. 00"
  records = append(reccords, rec2)
  
  fmt.Println(records)
  // package main
import "fmt"

func main() {
  records := make([][]string, 0)
  
  // record 1
  rec1 := make([]string, 4)
  rec1[0] = "Foster"
  rec1[1] = "Nathan"
  rec1[2] = "100.00"
  rec1[3] = "74. 00"
  records = append(reccords, rec1)
  
   // record 2
  rec2 := make([]string, 4)
  rec2[0] = "Dode"
  rec2[1] = "Lisa"
  rec2[2] = "92.00"
  rec2[3] = "96. 00"
  records = append(reccords, rec2)
  
  fmt.Println(records)
  // [[Foster Nathan 100.00 74. 00] [Dode Lisa 92.00 96. 00]]
}
```

Shorthand

```go
package main
import "fmt"

func main() {
  var rec1 []string{}		//use append to add items
  var records [][]string{}
  	
  fmt.Println(rec1)
  fmt.Println(records)
}
```

Incrementing a slice item

```go
package main
import "fmt"

func main() {
  mySlice := make([]int, 1)	// create a slice
  fmt.Println(mySlice[0])
  mySlice[0] = 7
  fmt.Println(mySlice[0])
  mySlice[0]++
  fmt.Println(mySlice[0])
}
```

## Map

- Key / Value storage
- A dictionary
- Init value is nil

```go
package main 
import "fmt"

func main() {
  // define a map op 1
  m := make(map[string]int)	// ["string": int]
  m["k1"] = 7
  m["k2"] = 13
  
  // define a map op 2
  n := map[string]int{
    "key1": 1, 
    "key2": 2
  }	// key string, value int
  
  // Deleting
  delete(n, 1) // -> [k1:7]
  
  // loo
  for key, val := range m {
    fmt.Println(key, " - ", val)
  }
}
```

## Struct

```go
package main
import "fmt"

type person struct {
    first string
  	last string
  	age int
}

func main() {
  p1 := person{"james", "bond", 20}
  p2 := person{"Miss", "moneypenny", 18}
  
  fmt.Println(p1.first, p1.last, p1.age)
  fmt.Println(p2.first, p2.last, p2.age)
}
```

Inheritance

```go
package main
import "fmt"

type Person struct {
    first string
  	last string
  	age int
}

type Zero struct {
    Person
  	first string
  	license bool
}

func main() {
  p1 := Zero{
    Person: Person{	// create a value of a type Person
      first: "james"
      last: "bond"
      age: 20
    },
    first: "Zero"
    license: true
  }
}
```

- In go, you don't create a class, you create a type
- In go, you don't instantiate, you create a value of a type

Tags

```go
type TaggerStruct struct {
  A int `datastore: "a,noindex"`
  B int `datastore: "b"`
  C int `datastore: ",noindex"`
}
```

Methods

```go
package main
import "fmt"

type Person struct {
    First string
  	Last string
  	age int
}

func (p Person) fullName() string {	//method
    return p.first + p.last
}

func main() {
  p1 := Person{"james", "bond", 20}
  
  fmt.Println(p1.fullName())
}
```

JSON

```go
package main
import ( 
  "fmt"
  "encoding/json"
)

type Person struct {
    First string	//Uppercase is exported
  	Last string
  	Age int
  	notExported int	//Lowercase is not exported
}

func main() {
  p1 := Person{"James", "Bond", 20, 007}
  bs, _ := json.Marshal(p1)	// slice of bytes, error
  fmt.Println(bs)			//[123 34 70 105 114 115... 125]
  fmt.Printf("%T \n", bs)	//[]unit8
  fmt.Println(string(bs))	//{"First":"James","Last":"Bond", "Age":20} -> notExported is not exported = Lowercase
}
```

Tagged JSON (Marshal)

```go
package main
import (
  "fmt"
  "encoding/json"
)

type Person struct {
  First string
  Last string `json:"-"`	//no value
  Age int `json:"wisdom score"`	//key name
}

func main() {
  p1 := Person{"James", "Bond", 20}
  bs, _ := json.Marshal(p1)
  fmt.Println(string(bs))	//{"First":"James","wisdom score":20}
}
```

Unmarshal

```go
package main
import (
  "fmt"
  "encoding/json"
)

type Person struct {
  First string
  Last string
  Age int	`json:"wisdom score"`	//tagged
}

func main() {
  var p1 Person
  
  bs := []byte(`{"First":"James", "Last":"Bond", "wisdom score":20}`)
  json.Unmarshal(bs, &p1)	// address reference to p1
  
  fmt.Println("----------------")
  fmt.Println(p1.First) 	// * not needed -> James
  fmt.Println(p1.Last)		// Bond
  fmt.Println(p1.Age)		// 20
  fmt.Printf("%T \n", p1)	// main.Person
}
```

Encoding / Decoding

```go
package main 

import(
  "encoding/json"
  "strings"
  "os"
)

type Person struct {
  First string
  Last string
  Age int
  notExported int
}

func main() {
  // Encode
  p1 := Person{"James", "Bond", 20, 007}
  json.NewEncoder(os.Stdout).Encode(p1)
  
  // Decode
  var p2 Person
  
  rdr := strings.NewReader(`{"First":"James", "Last":"Bond", "Age":20}`)
  json.NewDecoder(rdr).Decode(&p1)
}
```

## Interfaces

```go
package main

import (
	"fmt"
	"math"
)

//Square type struct
type Square struct {
	side float64
}

// Circle type struct
type Circle struct {
	radius float64
}

// Shape type interface -> any struct with method area()
type Shape interface {
	area() float64
}

func (s Square) area() float64 {
	return s.side * s.side
}

func (c Circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func info(z Shape) {
  // can take a circle or square
  // polymorphism
	fmt.Println(z)
	fmt.Println(z.area())
}
func main() {
	s := Square{10}
	c := Circle{5}

	info(s)
	info(c)
}
```

### Sort example

```go
package main

import (
	"fmt"
	"sort"
)

type People []string

func (p People) Len() int {
	return len(p)
}

func (p People) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p People) Less(i, j int) bool {
	return p[i] < p[j]
}

func main() {

	// Sort interface
	studyGroup := People{"Zero", "John", "Al", "Jenny"}

	fmt.Println(studyGroup)
	sort.Sort(studyGroup)
	fmt.Println(studyGroup)

	// Reverse interface
	sort.Sort(sort.Reverse(studyGroup))
	fmt.Println("Reverse interface: ", studyGroup)

	//Sort string
	s := []string{"Zeno", "John", "Al", "Jenny"}
	s2 := []string{"Zeno", "John", "Al", "Jenny"}
	// Method 1
	sort.Strings(s)
	fmt.Println("String ordered: ", s)
	// Method 2
	sort.StringSlice(s2).Sort()
	fmt.Println("String ordered 2: ", s2)
	// Reverse
	sort.Sort(sort.Reverse(sort.StringSlice(s2)))
	fmt.Println("Strings reversed: ", s2)

	//Sort ints
	n := []int{7, 4, 8, 2, 9, 19, 12, 32, 3}
	sort.Ints(n)
	fmt.Println("Ints sorted: ", n)
	// Reversed Ints
	sort.Sort(sort.Reverse(sort.IntSlice(n)))
	fmt.Println("Ints reversed: ", n)
}
```

- Empty interface{ } means that you cant put anything inside, not tided to any struct or interface

###Assertion

```go
package main
import "fmt"

func main() {
  var name interface{} = "Sydney"
  str, ok := name.(string)
  if ok {
    fmt.Printf("%T\n", str)
  } else {
    fmt.Printf("value is not string")
  }
}
```

## Concurrency

- **Concurrency** is the composition of _independently executing_ process. Is about dealing with many things at once. Doing many things, but only one at a time _"multitasking"_.
- **Parallelism** is the _simultaneous execution_ of computations. Is about doing lots of things at once. Doing many things at the same time.

**Concurrency**

```sequence
A->B:
B->C:
C->D:
```

**Parallelism**

```sequence
A->>B:
A->>C:
A->>D:
B->C:
B->D:
```



```go
package main
import (
  "fmt"
  "sync"
)

var wg sync.WaitGroup

func main() {
  wg.Add(2)
  go foo()
  go bar()
  wg.Wait()
}

func foo() {
  for i:=0; i<1000; i++ {
    fmt.Println("Foo: ", i)
  }
  wg.Done()
}


func bar() {
  for i:=0; i<1000; i++ {
    fmt.Println("Boo: ", i)
 }
  wg.Done()
}
```

Multiple Cores

```go
package main
import (
  "fmt"
  "sync"
  "runtime"
)

var wg sync.WaitGroup

func init() {
  // Init func -> special function in Go to set-up variables
  // run before main()
  runtime.GOMAXPROCS(runtime.NumCPU())
}

package main
import (
  "fmt"
  "sync"
)

var wg sync.WaitGroup

func main() {
  wg.Add(2)
  go foo()
  go bar()
  wg.Wait()
}

func foo() {
  for i:=0; i<1000; i++ {
    fmt.Println("Foo: ", i)
  }
  wg.Done()
}


func bar() {
  for i:=0; i<1000; i++ {
    fmt.Println("Boo: ", i)
 }
  wg.Done()
}


```

###Race Condition

```go
package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"time"
)

var wg sync.WaitGroup
var counter int64

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
	fmt.Println("Final counter:", counter)
}

func incrementor(s string) {
	for i := 0; i < 20; i++ {
		//atomic.AddInt64(&counter, 1)
		x := counter
		x++
		time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
		counter = x
		fmt.Println(s, i, "Counter:", counter)

	}
	wg.Done()
}
//go run --race main.go
```



###Mutex

_(Mutual exclusion)_

Prevent race conditions.

```go
package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"time"
)

var wg sync.WaitGroup
var counter int
var mutex sync.Mutex

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
	fmt.Println("Final counter:", counter)
}

func incrementor(s string) {
	for i := 0; i < 20; i++ {
		time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
		mutex.Lock()
		counter++
		fmt.Println(s, i, "Counter:", counter)
		mutex.Unlock()
	}
	wg.Done()
}

```

###Atomicity

```go
package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)

var wg sync.WaitGroup
var counter int64

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
	fmt.Println("Final counter:", counter)
}

func incrementor(s string) {
	for i := 0; i < 20; i++ {
		time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
		atomic.AddInt64(&counter, 1)
		fmt.Println(s, i, "Counter:", counter)

	}
	wg.Done()
}
```

### Channels

```go
package main
import (
  "fmt"
  "time"
)

func main() {
  c := make(chan int)	//unbuffered channel. (chan int, 123) -> buffered channel
  
  go func() {
    for i:=0; i<20; i++ {
        c <- i	// put number 1 on the channel and stop until off
    }
  }()
  
  go func() {
    for{
      fmt.Println(<- c) // off -> is ready to take a new value
    }
  }()
}
```

Wait Group

```go
package main
import (
  "fmt"
  "sync"
)

func main() {
  c := make(chan int)
  var wg sync.WaitGroup
  wg.Add(2)		//Add to go-routines
  
  go func() {
    for i:=0; i<10; i++ {
        c <- i
    }  
    wg.Done()
  }()
  
  go func() {
    for i:=0; i<10; i++ {
        c <- i
    }
    wg.Done()
  }()
  
  go func() {
    wg.Wait()	// wait till done
    close(c)	// if done, close channel
  }()
  
  for n:= range c {	// run only if channel is closed
    fmt.Println(n)
  }
}
```

Without sync

```go
package main
import "fmt"

func main(){
  c := make(chan int)
  done := make(chan bool)
  
  go func() {
    for i:=0; i<10; i++ {
        c <- i
    }
    done <- true
  }()
  
  go func() {
    for i:=0; i<10; i++ {
        c <- i
    }
    done <- true
  }()
  
  go func() {
    <-done
    <-done
    close(c)
  }()
  
  for n := range c {
    fmt.Println(n)
  }
}
```

Arguments / Return

```go
package main
import "fmt"

func main() {
  c := incrementor()
  cSum := puller(c)
  
  for n:= range cSum {
    fmt.Println(n)
  }
}

func incrementor() chan int {	//Return channel
  out := make(chan int)
  go func() {
    for i := 0; i<10; i++ {
        out <- i
    }
    close(out)
  }()
  return out
}

func puller(c chan int) chan int {	// Return and argument channel
  out := make(chan int)
  go func() {
    var sum int
    for n:=range c {
        sum += n
    }
    out <- sum
    close(out)
  }()
  return out
}
```

Select 

```go
package main
import (
  "fmt"
  "time"
)

func main() {
  select{
    case v1 := <- waitAndSend(3, 1):
    fmt.Println(v1)
    case v2 := <- waitAndSend(5, 2):
    fmt.Println(v2)
  }
}

func waitAndSend(v, i, int) chan int {
  c := make(chan int)
  go func(){
    time.Sleep(time.Duration(i) * time.Second)
    c <- v
  }()
  return c
}
```



## Error Handling

Standard Error functions.

```go
func main() {
  _, err := os.Open("no-file.txt")
  if err != nil {
    fmt.Prinln("err happened", err) // normal print
    log.Prinln("err happened", err) 
    log.Fatalln(err)
    panic(err)
  }
}
// log.Println -> print the standard logger. (date, error)
// log.Fataln -> call os.Exit(1) after writing the log msg
// panic -> give stack info and exit everything. Close deferred variables before exit.
```

Custom Errors

```go
package main
import (
  "log"
  "errors"
)

func main() {
  _,err := Sqrt(-10)
  if err != nil {
    log.Fatalln(err)
  }
}

func Sqrt(f float64) (float64, error) {
  if f < 0 {
    return 0, errors.New("norgate math: square root of negative number")
  }
  return 42, nil
}
```

Error variable

```go
package main
import (
  "log"
  "errors"
)

var ErrNorgateMath = errors.New("norgate math: square root of negative number")

func main() {
  _,err := Sqrt(-10)
  if err != nil {
    log.Fatalln(err)
  }
}

func Sqrt(f float64) (float64, error) {
  if f < 0 {
    return 0, ErrNorgateMath
  }
  return 42, nil
}
```

Extending Error()

```go
package main
import (
  "log"
  "fmt"
)

type NorgateMathError struct {
  lat, long string
  err error
}

func (n *NorgateMathError) Error() string {
  return fmt.Sprintf("a norgate error occured: %v %v %v", n.lat, n.lat, n.err)
}

func main() {
  _,err := Sqrt(-10)
  if err != nil {
    log.Println(err)
  }
}

func Sqrt(f float64) (float64, error) {
  if f < 0 {
    nme := fmt.Errorf("norgate math redux: square root of negative number: %v", f)
    return 0, &NorgateMathError{"50.22 N", "99.4 W", nme}
  }
  return 42, nil
}
```

