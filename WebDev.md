# Web Dev

## Templates

###Text templates

```go
package main
import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
  name := "Jess Maya"
  
  tpl := `
  	<!DOCTYPE html>
  	<html lang="en">
  	<head>
  	<meta charset="UTF-8">
  	<title>Hello World</title>
  	</head>
  	<body>
  	<h1>`+ name +`</h1>
  	</body>
  	</html>
  `
  fmt.Println(tpl)
  
  nf, err := os.Create("index.html")
  if err != nil {
    log.Fatal("error creating file", err)
  }
  defer nf.Close()
  io.Copy(nf, strings.NewReader(tpl))
}
```

####Template Files

```go
package main
import (
  "log"
  "os"
  "text/template"
)

func main() {
  tpl, err := template.ParseFiles("tpl.gohtml")
  if err != nil {
    log.Fatalln(err)
  }
  nf, err := os.Create("index.html")
  if err != nil {
    log.Println("error creating file", err)
  }
  defer nf.Close()
  
  err = tpl.Execute(nf, nil)
  if err != nil {
    log.Fatalln(err)
  }
}

```

####Multiple files

```go
package main
import (
  "log"
  "os"
  "text/template"
)

func main() {
  tpl, err := template.ParseGlob("templates/*.gohtml")
  if err != nil {
    log.Fatalln(err)
  }
  
  err = tpl.Execute(os.Stdout, nil)
  if err != nil {
    log.Fatalln(err)
  }
  
  // execute an especific template inside our *tpl
  err = tpl.ExecuteTemplate(os.Stdout, "one.gohtml", nil)
  if err != nil {
    log.Fataln(err)
  }
  
  err = tpl.ExecuteTemplate(os.Stdout, "two.gohtml", nil)
  if err != nil {
    log.Fatalln(err)
  }
}
```

#### Passing Data

```go
package main
import (
  "fmt"
  "os"
  "text/template"
)

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
  err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", 42)
  if err != nil {
    log.Fatalln(err)
  }
}
```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Parsing data</title>
  </head>
  <body>
    <h1>
      The meaning of life: {{.}}
    </h1>
  </body>
</html>
```

#### Variables on template

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Parsing data</title>
  </head>
  <body>
    {{$wisdom := .}}
    <h1>
      The meaning of life: {{$wisdom}}
    </h1>
  </body>
</html>
```

#### Passing data structures

##### Slices

```go
package main
import (
  "log"
  "os"
  "text/template"
)

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
  sages := []string{"Gandhi", "MLK", "Budha", "Jesus"}
  
  err := tpl.Execute(os.Stdout, sages)
  if err != nil {
    log.Fatalln(err)
  }
}
```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Parsing data</title>
  </head>
  <body>
    <ul>
      {{range .}}
      <li>{{.}}</li>
      {{end}}
    </ul>
    <ul>
      {{range $index, $element := .}}
      <li>{{$index}} - {{$element}}</li>
      {{end}}
    </ul>
  </body>
</html>
```

#####Map

```go
sages := map[string]string{
  "India": "Gandhi",
  "America": "MLK",
  "Meditate": "Buddha"
  "Love": "Jesus"
}
err := tpl.Execute(os.Stdout, sages)
```

```html
<ul>
  {{range $key, $val := .}}
  <li>{{$key}} - {{$val}}</li>
  {{end}}
</ul>

```

##### Struct

```go
type sage struct {
  Name string
  Motto string
}

func main(){
  ...
  buddha := sage {
    Name: "Buddha",
    Motto: "The belief or no beliefs"
  }
  err := tpl.Execute(os.Stdout, buddha)
}

```

```html
<ul>
  <li>{{.Name}} - {{.Motto}}</li>
</ul>
```

##### Slice of struct

```go
type sage struct {
  Name string
  Motto string
 }

func main() {
  ---
  buddha := sage {
    Name: "Buddha",
    Motto: "The belief or no beliefs"
  }
  gandhi := sage {
    Name: "Gandhi",
    Motto: "Be the change"
  }
  mlk := sage {
    Name: "Martin Luther King",
    Motto: "Hated never ceases with hatred, but with love alone is healed."
  }
  sages := []sage{buddha, gandhi, mlk}
  err := tpl.Execute(os.Stdout, sages)
}
```

```html
<ul>
  {{range .}}
  <li>{{.Name}} - {{.Motto}}</li>
  {{end}}
</ul>
```

##### Struct slice struct

```go
type sage struct {
  Name string
  Motto string
}

type car struct {
  Manufacturer string
  Model string
  Doors int
}

type items struct {
  Wisdom []sage
  Transport []car
}

func main() {
    ...
  b := sage {
    Name: "Buddha",
    Motto: "The belief of no beliefs"
  }
  g := sage {
    Name: "Gandhi",
    Motto: "Be the change"
  }
  f := car {
    Manufacturer: "Ford",
    Model: "F150",
    Doors: 2
  }
  c := car {
    Manufacturer: "Toyota",
    Model: "Corolla",
    Doors: 4
  }
  
  sages := []sage{b, g}
  cars := []car{f, c}
  // op 1
  data := items {
    Wisdom: sages,
    Transport: cars
  }
  // op 2
  data := struct {
    Wisdom []sage
    Transport []car
  }{
    sages,
    cars
  }
  
  err := tpl.Execute(os.Stdout, data)
}
```

```html
<ul>
  {{range .Wisdom}}
  <li>{{.Name}} - {{.Motto}}</li>
  {{end}}
</ul>
<ul>
  {{range .Transport}}
  <li>{{.Manufacturer}} - {{.model}} - {{.Doors}}</li>
  {{end}}
</ul>
```

##### Functions in templates

```go
var fm = template.FuncMap {
  "uc": strings.ToUpper,
  "ft": firstThree
}

func firstThree(s string) string {
  s = strings.TrimSpace(s)
  s = s[:3]
  return s
}

func init() {
  tpl = template.Must(template.New("").Funcs(fm).ParseFiles("tpl.gohtml"))
}

func main() {
    ....
}
```

```html
<body>
  {{range .Wisdom}}
  {{uc .Name}}
  {{end}}
  
  {{range .Wisdom}}
  {{ft .Name}}
  {{end}}
</body>
```

### Date formating

```go
package main
import (
  "log"
  "os"
  "text/template"
  "time"
)
var tpl *template.Template

func init() {
  tpl = template.Must(template.New("").Funcs(fm).Parse("tpl.gohtml"))
}

func monthDayYear(t time.Time) string {
  return t.Format("01-02-2006")
}

var fm = template.FuncMap {
  "fdateMDY": monthDayYear
}

func main() {
  err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", time.Now())
  if err != nil {
    log.Fatalln(err)
  }
}
```

```html
<body>
  Without date formatting: {{.}}
  With date formatting: {{fdateMDY .}}
</body>
```

##### Pipelining

```go
func init() {
    tpl = template.Must(template.New("").Funcs(fm).ParseFiles("tpl.gohtml"))
}

func double(x int) int {
    return x + x
}

func square(x int) float64 {
  return math.Pow(float64(x), 2)
}

func sqRoot(x float64) float64 {
  return math.Sqrt(x)
}

var fm = template.FuncMap {
  "fdbl": double,
  "fsq": square,
  "fsqrt": sqRoot
}

func main() {
  err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", 3)
}
```

```html
<body>
  {{ . }}						<!-- 3 -->
  {{. | fdbl }}					<!-- 6 -->
  {{. | fdbl | fsq }}			<!-- 36 -->
  {{. | fdnl | fsq | fsqrt  }}	<!-- 6 -->
</body>
```

##### Global functions

```go
package main
import (
  "log"
  "os"
  "text/template"
)

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
  xs := []string{"zero", "one", "two", "three", "four", "five"}
  err := tpl.Execute(os.Stdout, xs)
  if err != nil {
    log.Fatalln(err)
  }
}
```

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Parsing data</title>
  </head>
  <body>
    {{ index . 0 }}	<!-- Index position of 0 in current data-->
    {{ index . 1 }}
    {{ index . 2 }}
    <!-- 
	index = a function you can use in a template, it is a "predefined global function"
	Returns the result of indexing its first argument by following arguments. Thus "index x 1 2 3" is, in Go syntax, x[1][2][3]. Each indexed item must be a map, slice or array.
	-->
    <!-- Comment in templates-->
    {{/*(index .Wisdom 3).Name*/}}
  </body>
</html>
```

##### Nesting templates

_(Modular templates)_

######define:

```go
{{define "TemplateName"}}
insert content here
{{end}}
```

######use:

```go
{{template "TemplateName"}}
```

Example

```html
<!-- tpls.gohtml -->
{{define "polarbear"}}
Here is a polar bear
{{end}}
```

```html
<!-- index.gohtml -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Parsing data</title>
  </head>
  <body>
    <p>
      {{template "polarbear"}}
    </p>
  </body>
</html>
```

```go
package main
import (
  "log"
  "os"
  "text/template"
)

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func main() {
  err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", nil)
  if err != nil {
    log.Fatalln(err)
  }
}

```

###### Passing data to template

```html
{{define "polarbear"}}
Here is a polar bear data: {{.}}
{{end}}
```

```html
<p>
  {{template "polarbear" .}}
</p>
```

```go
func main() {
  err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", 42)
  if err != nil {
    log.Fatalln(err)
  }
}
```

##Server

###HTTP Request

Request

- request line
- headers
- optional message body

Request line

- Method SP Request-URI SP HTTP-Version CRLF

Example

- GET /path/to/file/index.html HTTP/1.0

###HTTP Response

Response

- status line
- headers
- optional message body

Status line

- HTTP-Version SP Status-Code SP Reason-Phrase CRLF

Example

- HTTP/1.0 200 OK

### TCP Server

```go
package main
import (
  "fmt"
  "io"
  "log"
  "net"
)

func main() {
  li, err := net.Listen("tcp", ":8080")
  if err != nil {
    log.Panic(err)
  }
  defer li.Close()
  
  for {
    conn, err := li.Accept()
    if err !=nil {
      log.Println(err)
    }
    io.WriteString(conn, "\nHello from TCP server\n")
    fmt.Fprintln(conn, "How is your day?")
    fmt.Fprintf(conn, "%v", "Well, I hope!")
    
    conn.Close()
  }
}
```

Test:

```sh
telnet localhost 8080
```

Phases:

- Listen
- Accept (connection)
- Read & Write to Connection

#### Bufio

```go
package main
import (
  "bufio"
  "fmt"
  "log"
  "net"
)

func main() {
  li, err := net.Listen("tcp", ":8080")
  if err != nil {
    log.Panic(err)
  }
  defer li.Close()
  
  for {	//always listening
    conn, err := li.Accept()
    if err != nil {
      log.Println(err)
    }
    go handle(conn)	//gorutine -> Multiple connections may be served concurrently
  }
}

func handle(conn net.Conn) {
  scanner := bufio.NewScanner(conn)
  for scanner.Scan() {
    ln := scanner.Text()
    fmt.Println(ln)
  }
  defer conn.Close()
}
```

## HTTP Server

###Handler

```go
type Handler interface {
  ServeHTTP(ResponseWriter, *Request)
}
```

### Server

http.ListenAndServe

```go
func ListenAndServe(addr string, handler Handler) error
```

http.ListenAndServeTLS

```go
func ListenAndServeTLS(addr, certFile, keyFile, handler Handler) error
```

### Request

http.Request

```go
type Request struct {
  Mehotd string
  URL *url.URL
  // Header = map[string][]string{
  // "Accept-Encoding": {"gzip, deflate"}
  // "Accept-Language": {"en-us"}
  // "Foo": {"bar "}
  //}
  Header Header
  Body io.ReadCloser
  ContentLength int64
  Host string
  // This field is only available after ParseForm is called
  Form url.Values
  // This field is only available after ParseForm is called
  PostForm url.Values
  MultipartForm *multipart.Form
  RemoteAddr string
}
```

### Response

http.ResponseWriter

```go
type ResponseWriter interface {
  // Header returns the header map that will be sent by WriteHeader.
  Header() Header
  // Write writes the data to the connection as part of an HTTP onlu
  Write([]byte) (int, error)
  // WriteHeader sends an HTTP response header with status code.
  // WriteHeader(http.StatusOK)
  WriteHeader(int)
}
```

```go
package main
import (
  "fmt"
  "net/http"
)

type hotdog int

// adding type hotdog as a handler type
func (m hotdog) ServeHTTP(res http.ResponseWriter, req *http.Request) {
  res.Header().Set("Mcleod-key", "this is  from mcleod")
  res.header().Set("Content-Type", "text/html; charset=utf-8")
  fmt.Fprintln(res, "<h1>Any code you want in this func</h1>")
}

func main() {
  var d hotdog
  http.ListenAndServe(":8080", d)
}
```

### ServeMux

_(Multiplexer Router)_

Is an HTTP request multiplexer. A ServeMux matches the URL of each incoming request against a list of registered patterns and calls the handler for the pattern that most closely matches the URL.

- **ServeMux**: 
  - NewServeMux
    - We can create a *ServeMux by using NewServeMux
  - default ServeMux
    - We can use the default ServeMux by passing **nil** to ListenAndServe
- **Handle**
  - Takes a value of type **Handler**
- **HandleFunc**
  - Takes a func with this signature: **func( ResponseWriter, *Request)**

```go
// No Mux
package main
import(
  "io"
  "net/http"
)

type hotdog int

func (m hotdog) ServeHTTP(res http.ResponseWriter, req *http.Request) {
  switch req.URL.path {
    case "/dog":
      io.WriteString(res, "Doggy Dog")
    case "/cat":
      io.WriteString(res, "Kitty Cat")
  }
}

func main() {
  var d hotdog
  http.ListenAndServe(":8080", d)
}
```

```go
package main
import (
  "io"
  "net/http"
)

type hotdog int

func (d hotdog) ServeHTTP(res http.ResponseWriter, req *http.Request) {
  io.WriteString(res, "Doggy Dog")
}

type hotcat int

func (c hotcat) ServeHTTP(res http.responseWriter, req *http.Request) {
  io.WriteString(res, "Kitty Cat")
}

func main() {
  var d hotdog
  var c hotcat
  
  mux := http.NewServeMux() //mux constructor
  mux.Handle("/dog/", d) // with / at last -> response to path /dog and /dog/whaterever/you/want
  mux.Handle("/cat", c) //exact match -> no last /
  //cat just serve "/cat" and no "/cat/something/else"
  
  http.ListenAndServe(":8080", mux)
}
```

####Default ServeMux 

_(best practice)_

```go
package main
import (
  "io"
  "net/http"
)


func d (res http.ResponseWriter, req *http.Request) {
  io.WriteString(res, "Doggy Dog")
}

func c (res http.responseWriter, req *http.Request) {
  io.WriteString(res, "Kitty Cat")
}

func main() {
  http.HandleFunc("/dog/", d)
  http.HandleFunc("/cat", c)
  http.ListenAndServe(":8080", nil) // if handler is nil -> get use the default
}
```

#### Third-Party:  Julien Schmidt's router

```go
package main
import(
  "fmt"
  "github.com/julienschmidt/httprouter"
  "html/template"
  "log"
  "net/http"
)

var tpl *template.Template

func init() {
  tpl = template.Must(template.ParseGlob("templates/*"))
}

func main() {
  mux := httprouter.New()
  mux.GET("/", index)
  mux.GET("/apply", apply)
  mux.POST("/apply",applyProcess)
  mux.GET("/user/:name", user)
  mux.GET("blog/:category/:article", blogRead)
  mux.POST("blog/:category/:article", blogWrite)
  http.ListenAndServe(":8080", mux)
}

func index(w http.ResponseWriter, req *http.Request) {
  err := tpl.ExecuteTemplate(w, "index.gohtml", nil)
  HandleError(w, err)
}

func about(w http.ResponseWriter, req *http.Request) {
  err := tpl.ExecuteTemplate(w, "about.gohtml", nil)
  HandleError(w, err)
}

func apply(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
  err := tpl.ExecuteTemplate(w, "apply.gohtml", nil)
  HandleError(w, err)
}

func applyProcess(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
  err := tpl.ExecuteTemplate(w, "applyProcess.gohtml", nil)
  HandleError(w, err)
}

func user(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
  fmt.Fprintf(w, "USER, %s!\n", ps.ByName("name"))
}
```

#### HTTP.FileServer

```go
package main
import(
  "io"
  "net/http"
)

func main() {
  http.Handle("/", http.FileServer(http.Dir("."))) // return a handler
  http.HandleFunc("/dog", dog)
  http.ListenAndServe(":8080", nil)
}

func dog(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Content-Type", "text/html; charset=utf-8")
  io.WriteString(w, `<img src="toby.jpg" />`)
}
```

##### StripPrefix

```go
package main
import(
  "io"
  "net/http"
)

func main() {
  http.Handle("/", http.FileServer(http.Dir("."))) // return a handler
  http.HandleFunc("/dog", dog)
  http.Handle("/resources/", http.StripPrefix("/resources", http.Dir("./assets")))
  //Handle strip "/resources/toby.jpg" -> "/toby.jpg" = search for file in "./assets/toby.jpg"
  http.ListenAndServe(":8080", nil)
}

func dog(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Content-Type", "text/html; charset=utf-8")
  io.WriteString(w, `<img src="/resources/toby.jpg" />`)
}
```

#### Static file server

```go
package main
import "net/http"

func main() {
  http.ListenAndServe(":8080", http.FileServer(http.Dir(".")))
}
```

#### log.Fatal

```go
package main
import "net/http"

func main() {
  log.Fatal(http.ListenAndServe(":8080", http.FileServer(http.Dir("."))))
  // http.ListenAndServe returns an error
  // log.Fatal saves the error to log file
}
```

#### NotFoundHandler

```go
package main
import (
  "fmt"
  "net/http"
)

func main() {
  http.HandleFunc("/", dog)
  http.Handle("/favicon.ico", http.NotFoundHandler())
  http.ListenAndServe(":8080", nil)
}

func dog(w http.responseWriter, req *Request) {
  fmt.Println(req.URL)
  fmt.Fprintln(w, "go look at your terminal")
}
```

## State

We can pass values from the client to the server through the URL or through the body of the request.

When you submit a form, you can use either the "post" or "get" method. The "post" method sends the form submission through the body of the request. The "get" method for a form submission sends the  form values through the url.

- **Post** has four letters and so does **body**
- **Get** has three letters and so does **url**

#### Passing data through URL

```html
htpps://video.google.co.uk:80/videoplay?docid=12345#00h123
```

- protocol: https://
- subdomain: video.
- domain name: google.co.uk
- port: :80
- path: /videoplay
- query: ?
- parameters: docid=12345
- fragment: #00h123

```go
package main
import (
  "io"
  "net/http"
)

func main() {
  http.HandleFunc("/", foo)
  http.Handle("/favicon.ico", http.NotFoundHandler())
  http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
  v := req.FormValue("q")	//dog
  io.WriteString(w, "My search: "+v)
} 
```

```bash
http://localhost:8080/?q=dog
```

#### Passing Values from Form

```go
package main
import (
  "io"
  "net/http"
)

func main() {
  http.HandleFunc("/", foo)
  http.Handle("/favicon.ico", http.NotFoundHandler())
  http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
  v := req.FormValue("q")
  w.Header().Set("Content-Type", "text/html; charset=utf-8")
  io.WriteString(w,
                `<form method="post">
                	<input type="text" name="q"/>
                    <input type="submit"/>
                </form>` + v
                )	//try changing the method to get -> check the url
} 
```

 #### Upload, Creating and Creating a File

```go
package main
import (
  "io"
  "net/http"
)

func main() {
  http.HandleFunc("/", foo)
  http.Handle("/favicon.ico", http.NotFoundHandler())
  http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
  var s tring
  fmt.Println(req.Method)
  if req.Method == http.MethodPost {
    //open
    f, h, err := req.FormFile("q")
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    defer f.Close()
    
    fmt.Println("\nfile: ", f, "\nheader: ", h, "\nerr", err)
    //read
    bs. err := ioutil.ReadAll(f)
    if err != nil {
      http.Error(w, err.Error(),  http.StatusInternalServerError)
      return
    }
    s = string(bs)
    
    //Store on server
    dst, err := os.Create(filepath.Join("./user/", h.Filename))
    if err != nil {
        http.Error(w, err.Error(),  http.StatusInternalServerError)
      return
    }
    defer dst.Close()
    
    _, err = dst.Write(bs)
    if err != nil {
        http.Error(w, err.Error(),  http.StatusInternalServerError)
    }
  }
  
  w.Header().Set("Content-Type", "text/html; charset=utf-8")
  io.WriteString(w,
                `<form method="POST" enctype="multipart/form-data">
                	<input type="file" name="q"/>
                    <input type="submit"/>
                </form>` + s
                )
} 
```

##### Enctype

- Default: application/x-www-form-urlencoded
  - first=james&last=Bond
- multipart/form-data. _(use it when your form includes any type file)_
- text/plain
  - first=james last=bond

###Redirect

```go
func bar(w http.ResponseWriter, req *http.Request) {
  // op 1
  w.Header().Set("Location", "/")
  w.WriteHeader(http.StatusSeeOther)
  // op 2
  http.Redirect(w, req, "/", http.StatusSeeOther)
}
```

### Cookies

```go
package main
import (
  "fmt"
  "net/http"
)

func main() {
  http.HandleFunc("/", set)
  http.HandleFunc("read", read)
  http.ListenAndServe(":8080", nil)
}

func set(w http.ResponseWriter, req *http.Request) {
  http.SetCoockie(w, &http.Coockie {
    Name: "my-cookie",
    Value: "some value"
  })
}

func read(w http.ResponseWriter, req *http.Request) {
  c, err := req.Cookie("my-cookie")
  if err != nil {
    http.Redirect(w, req, "/", http.StatusSeeOther)
    return
  }
  fmt.Fprintln(w, "Your cookie: ", c)
}
```

####Updating cookie

```go
func foo( res http.ResponseWriter, req *http.Request) {
  cookie , err := req.Cookie("my-cookie")
  
  if err == http.ErrNoCookie { // if cookie exists
    cookie = &http.Cookie {
      Name: "my-cookie",
      Value: "0"
    }
  }
  
  count, err := strconv.Atoi(cookie.Value)
  if err != nil {
    log.Fatalln(err)
  }
  count++
  cookie.Value = strconv.Itoa(count)
  
  http.SetCookie(res, cookie)
  io.WriteString(res, cookie.Value)
}
```

#### Delete a cookie

```go
func expire(w http.ResponseWriter, req *http.Request) {
  c, err := req.Cookie("session")
  if err != nil {
    http.Redirect(w, req, "/set", http.StatusSeeOther)
    return
  }
  
  c.MaxAge = -1 //delete a cookie
  http.SetCookie(w, c)
  http.Redirect(w, req, "/", http.StatusSeeOther)
}
```



